'use strict';

module.exports = addr2coords;

const https = require('https');
const config = require(`${__dirname}/config`);

function addr2coords(address, callback) {
  let options = makeRequestOptions(address, config.GOOGLE_MAPS_HOST,
      config.GOOGLE_MAPS_PATH, config.GOOGLE_MAPS_KEY);

  return https.get(options, response => {
    var data = '';

    response.on('data', chunk => data += chunk);

    response.on('end', () => {
      try {
        callback(null, {
          address: address,
          coords: JSON.parse(data)
        });

      } catch (e) {
        callback(e, null);
      }
    });

    response.on('error', error => {
      callback(error, null);
    });
  });
}

function makeRequestOptions(address, host, path, key) {
  return {
    host: host,
    path: `${path}?key=${key}&address=${encodeAddress(address)}`,
  };
}

function encodeAddress(address) {
  return encodeURIComponent(address.replace(/ /g, '+'));
}
