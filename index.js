'use strict';

const fs = require('fs');
const readline = require('readline');
const addr2coords = require(`${__dirname}/addr2coords`);

(function main(argv) {
  const args = parseArgs(argv);
  const rl = readline.createInterface({
    input: fs.createReadStream(args['addressBook'])
  });

  rl.on('line', address => addr2coords(address, showCoords));
})(process.argv);

function parseArgs(argv) {
  if (argv.length !== 3) {
    console.error(`usage: ${argv[0]} ${argv[1]} ADDRESS_BOOK`);
    process.exit(1);
  }

  return { addressBook: argv[2] };
}

function showCoords(error, result) {
  if (error) {
    console.error(`ERROR: ${error.message}`);
    return;
  }

  const {address, coords} = result;

  if (coords['results'].length > 0) {
    const location = coords['results'][0]['geometry']['location'];
    console.log(`${address};${location['lat']};${location['lng']}`);

  } else {
    console.log(`${address};not found;not found`);
  }
}
