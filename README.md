# addr2coords

Cli-based application to convert locations from address to coordinates via
Google Maps' Geocode API.

## Installation

 - Install [NodeJs](https://nodejs.org/en/) (v6.5.0)
 - Get addr2coords
 - Add your Geocode API key to the ```config.js``` file

```
$ git clone https://gitlab.com/allanmatheus/addr2coords.git
$ cd addr2coords
$ cat config.js
'use strict';

module.exports = {
  GOOGLE_MAPS_KEY: '<YOUR-GEOCODE-API-KEY>',
  GOOGLE_MAPS_HOST: 'maps.googleapis.com',
  GOOGLE_MAPS_PATH: `/maps/api/geocode/json`
};
$ vi config.js # Add your geocode api key
```

## Run

```
$ npm run addr2coords ADDRESSES
$ # or
$ node index.js  ADDRESSES
```

## Input format

Plain text file. One address per line.

Example:

```
Petrópolis - RJ, 25640-030, Brasil
Petrópolis - RJ, 25620-000, Brasil
Petrópolis - RJ, 25620-150, Brasil
Petrópolis - RJ, 25620-100, Brasil
Petrópolis - RJ, 25685-330, Brasil
Petrópolis - RJ, 25725-029, Brasil
Petrópolis - RJ, 25730-730, Brasil
Petrópolis - RJ, 25620-150, Brasil
Petrópolis - RJ, 25660-004, Brasil
```
