'use strict';

module.exports = {
  GOOGLE_MAPS_KEY: '<YOUR-GEOCODE-API-KEY>',
  GOOGLE_MAPS_HOST: 'maps.googleapis.com',
  GOOGLE_MAPS_PATH: `/maps/api/geocode/json`
};
